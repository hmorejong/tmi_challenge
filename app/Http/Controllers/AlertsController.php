<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Orchestra\Parser\Xml\Facade as XmlParser;


class AlertsController extends Controller
{
    public function index()
    {
        $feeds = [];
        $files = \File::files(storage_path().'/feeds'); // files from storage folder

        foreach ($files as $filename){

            $xml_feed = $this->parseXMLFile($filename);
            $feeds[] = [ 'FileDate' => substr($filename, -10,6), 'PriceDate' => $xml_feed['PriceDate'], 'Region' => $xml_feed['Region'], 'TotalSectors' => sizeof($xml_feed['Sectors'])];
        }
        return view('alerts.index', compact('feeds'));
    }

    public function uploadXMLFiles(Request $request){

        //Uploading XMl feeds
        $files = $request->allFiles('attachments');

        foreach ($files['uploads'] as $file)
        {
            $name=$file->getClientOriginalName();
            $path = storage_path().'/feeds/';
            $file->move($path, $name);
        }
        return redirect('/');
    }

    public function show($date = NULL)
    {
        //Validation for date URL parameter

        $alerts = $this->generateAlerts($date);
        return view('alerts.show', compact('alerts'));
    }

    /*
     * Assuming date parameter with format 'ymd' same as PriceDate
     */
    private function generateAlerts($current_date = null)
    {
        if(!isset($current_date))
            $current_date = date('ymd');

        $previous_date = $this->renderDate($current_date, '-1 day');  // previous day from current date
        $alerts = ['current_date' => $this->renderDate($current_date),'previous_date' => $this->renderDate($previous_date), 'alerts' => []];

        $files = \File::files(storage_path().'/feeds'); // files from storage folder
        $key_current = key(preg_grep("/\b$current_date\b/i", $files));
        $key_previous = key(preg_grep("/\b$previous_date\b/i", $files));

        if(isset($key_current) && isset($key_previous))
        {
            // Filename for previous day and current feed
            $previous_filename = $files[$key_previous];
            $current_filename = $files[$key_current];

            //Parsing XML file content of the previous day
            $xml_feed = $this->parseXMLFile($previous_filename);
            $feeds[$previous_date] = $this->renderXMLarray($xml_feed);

            //Parsing XML file content of the current day
            $xml_feed = $this->parseXMLFile($current_filename);
            $feeds[$current_date] = $this->renderXMLarray($xml_feed);

            // Loop through current date XML feed
            foreach($feeds[$current_date]['Sectors'] as $sector => $feed)
            {
                // Validating each Sector feed exists in the previous XML feed
                if(isset($feeds[$previous_date]['Sectors'][$sector]))
                {
                    $current_PFDirection = $feed['PFDirection'];
                    $previous_PFDirection = $feeds[$previous_date]['Sectors'][$sector]['PFDirection'];

                    $previous_sector = $feeds[$previous_date]['Sectors'][$sector]['SectorId'];

                    // Generating alerts from deltas on PFDirection by SectorId from previous to current day
                    if($previous_sector == $sector && $current_PFDirection != $previous_PFDirection)
                    {
                        $alerts['alerts'][$sector]['current_PFDirection'] = $current_PFDirection;
                        $alerts['alerts'][$sector]['previous_PFDirection'] = $previous_PFDirection;
                        $alerts['alerts'][$sector]['sector'] = $feeds[$current_date]['Sectors'][$sector]['Name'];
                    }
                }
                // Validating missing sectors on previous XML feed
                else
                    $alerts['errors'][$sector] = $feeds[$current_date]['Sectors'][$sector]['Name'];
            }
            return $alerts;
        }
        else
            return false;
    }

    //XML file parser from local storage filename
    private function parseXMLFile($filename){

        //Parsing XML file content of the previous day
        $xml_feed = XmlParser::load($filename);

        return $xml_feed->parse([
            'PriceDate' => ['uses' => 'PriceDate'],
            'Region' => ['uses' => 'Region'],
            'Sectors' => ['uses' => 'Sector[SectorId,Name,BullPC,PFFormation,PFFormationDate,PFSignal,PFDirection,PFReversalDate,PFRelSignal,PFRelDirection,PFRelReversalDate,WK30Value,WK30Direction,ChartId,IndexChartId,Current,Prev,Change,Stocks,ETFs]']
        ]);
    }

    // Calculates previous day in format 'ymd'
    private function renderDate($date, $time = NULL){

        $day = substr($date, -2,2);
        $month = substr($date, -4,2);
        $year = substr($date, 0,2);

        if(isset($time))
            $date = date('ymd',(strtotime($time,strtotime($month.'/'.$day.'/'.$year))));
        else
            $date = date('Y-m-d',(strtotime($month.'/'.$day.'/'.$year)));

        return $date;
    }

    // Renders array from XML parser updating sectors key from auto increment id to SectorId
    private function renderXMLArray($feeds){

        $new_feed['PriceDate'] = $feeds['PriceDate'];
        $new_feed['Region'] = $feeds['Region'];
        $new_feed['Sectors'] = [];

        foreach ($feeds['Sectors'] as $key => $sector)
            $new_feed['Sectors'][$sector['SectorId']] = $sector;

        return $new_feed;
    }

}
