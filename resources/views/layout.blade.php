<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="XML Parser generates alerts for investment sectors comparing PFDirection value of each Sector from XML feeds.">
    <meta name="author" content="Hanni Morejon">

    <title>Investor Intelligence XML Parser -  True Market Insiders</title>

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Optional theme -->
    <link href="{{ asset('css/jumbotron-narrow.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet">

</head>
<body>


<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="{{ url('/') }}">Home</a></li>
                <li role="presentation"><a href="{{ url('/about') }}">About</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">True Markets Insiders</h3>
    </div>

    @yield('content')

    <footer class="footer">
        <p>&copy; True Market Insiders 2018 </p>
    </footer>

</div> <!-- /container -->

<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fileinput.min.js') }}"></script>

@yield('scripts')

</body>

</html>

