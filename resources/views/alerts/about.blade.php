@extends('layout')

@section('content')

    <h3>About XML Parser</h3>

    <p>
        The XML Parser is an application for investment market. It provides a feature that generates alerts for sectors based on the XML feeds. <br>
        Feeds from XML are parsed and generated when the XML files are uploaded. Sectors deltas for investment takes place when the PFDirection value of each Sector from XML feeds are different than the previous day feed.
    </p>
    <br>

    <h5>Assumptions:</h5>
    <ul>
        <li>Files uploaded will be only XML type.</li>
        <li>XML files should have the correct header and format in order to be parsed.</li>
        <li>Alerts are displayed when:<ul> <li> Previous date (meaning day before) and current date XML file were uploaded previously. </li> <li>PFDirection deltas from within current and previous date for any sector are found.</li> <li> URL parameter for date filter should correspond with format 'ydm'.</li> </ul> </li>
    </ul>

@stop
