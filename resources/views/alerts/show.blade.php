@extends('layout')

@section('content')

    <div class="row marketing">

        @if($alerts)
        <h4>Alerts for feed from {{ $alerts['previous_date'] }} to {{ $alerts['current_date'] }}</h4>

            @if (count($alerts['alerts']))
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Sector Id</th>
                        <th>Current PF Direction</th>
                        <th>Previous PF Direction</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($alerts['alerts'] as $k => $alert)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $alert['sector'] }}</td>
                        <td>{{ $alert['current_PFDirection'] }}</td>
                        <td>{{ $alert['previous_PFDirection'] }}</td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            @else
                <div class="alert alert-success" role="alert">No alerts from current and previous feeds.</div>
            @endif
            @if(isset($alerts['error']) && count($alerts['errors']))
                @foreach($alerts['errors'] as $sector => $error)
                    <div class="alert alert-warning" role="alert">Warning! Missing section <b>{{ $error }}</b> info on previous XML feed.</div>
                @endforeach
            @endif

        @else
            <div class="alert alert-danger" role="alert">Oh snap! Missing feed files to generate alerts, try upload the missing file.</div>
        @endif

@stop