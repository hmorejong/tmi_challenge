@extends('layout')

@section('content')

    <div class="jumbotron">
        <h1>XLM Parser</h1>
        <p class="lead">Feeds from XML are parsed and generated onces XML files are uploaded. Sectors deltas for investment takes place when the PFDirection value of each Sector from XML feeds is different from the previous day.</p>
        <p><button type="button" class="btn btn-lg btn-success" role="button" data-toggle="modal" data-target="#myModal" >Upload Files</button></p>
    </div>

    <div class="row marketing">
        <div class="col-lg-6">

            @foreach($feeds as $k => $feed)
                @if ($k % 2 == 0)
                    <a href="{{ url('/alerts/show/'. $feed['FileDate']) }}" class="link"><h4>Feed {{ $feed['PriceDate'] }}</h4></a>
                    <p>
                        <ul>
                            <li>Region: {{ $feed['Region'] }}</li>
                            <li>Total Sectors: {{ $feed['TotalSectors'] }}</li>
                        </ul>
                    </p>
                @endif
            @endforeach
        </div>

        <div class="col-lg-6">
            @foreach($feeds as $k => $feed)
                @if ($k % 2 != 0)
                    <a href="{{ url('/alerts/show/'. $feed['FileDate']) }}" class="link"><h4>Feed {{ $feed['PriceDate'] }}</h4></a>
                    <p>
                        <ul>
                            <li>Region: {{ $feed['Region'] }}</li>
                            <li>Total Sectors: {{ $feed['TotalSectors'] }}</li>
                        </ul>
                    </p>
                @endif

                @endforeach
            </div>
        </div>


    <!-- Modal -->
    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload XML files</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12" style="padding:20px 20px 50px 50px">
                            <form action="{{ url('upload') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input id="input-1"  name="uploads[]" type="file" class="file" data-show-upload="true" data-show-caption="true" multiple>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

@stop